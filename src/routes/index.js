const express = require('express');
const router = express.Router();
const ExchangeController = require('../controllers/exchange');

router.get('/', ExchangeController.listPrices);
router.get('/conversion', ExchangeController.convertCurrency);

module.exports = router;
