const { PORT } = require('./config');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const routes = require('./routes/index');
const cache = require('./controllers/dataSource/pricesCache');
let pricesCache = new cache().getInstance()
pricesCache.buildCache()

const app = express();
app.use(logger('dev'));
app.use(helmet());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(routes);


const server = app.listen(PORT, () => {
    console.log(`Server listen at port ${PORT}.`);
});

module.exports = server