const dataAbstract = require('./dataAbstract')
const request = require('request');
const { API_ID } = require('../../config');

class openExchangeApi extends dataAbstract {

    getDataFromApi = () => {
        return new Promise(resolve => {
            request(`https://openexchangerates.org/api/latest.json?app_id=${API_ID}`, function (error, response, body) {
                console.error('error:', error); // Print the error if one occurred
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                let data = JSON.parse(body)
                resolve({ rates: data.rates, timestamp: data.timestamp * 1000 })
            });

        })

    }


    async getPrices() {
        return this.getDataFromApi()
    }

}


module.exports = openExchangeApi