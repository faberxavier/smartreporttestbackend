const dataAbstract = require('./dataAbstract')
fs = require('fs');
const path = './cache.data'

let memoryCache

class pricesCache extends dataAbstract {

    async buildCache() {
        try {
            if (fs.existsSync(path)) {
                fs.readFile(path, 'utf8', (err, data) => {
                    if (err) {
                        console.log(err)
                        memoryCache = { timestamp: 0 }
                    }
                    else {
                        data.length > 0 ? memoryCache = JSON.parse(data) : memoryCache = { timestamp: 0 }
                    }
                })
            }
            else {
                fs.writeFile(path, JSON.stringify({ timestamp: 0 }), (err) => {
                    if (err)
                        console.log(err)
                    memoryCache = { timestamp: 0 }
                })
            }

        } catch (error) {
            console.log(error)
        }
    }
    buildCacheInMemory(data) {
        memoryCache = data
    }

    persistData(data = { timestamp: 0 }) {
        fs.writeFile(path, JSON.stringify(data), (err) => {
            if (err)
                console.log(err)
        })

    }
    updateCache(data) {
        this.persistData(data)
    }
    getTime() {
        return memoryCache.timestamp
    }
    getPrices() {
        return memoryCache
    }
}

class Singleton {

    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new pricesCache();

        }
    }

    getInstance() {
        return Singleton.instance;
    }

}

module.exports = Singleton;

