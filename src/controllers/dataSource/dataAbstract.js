class dataAbstract {
    constructor() {
        if (this.constructor === dataAbstract) {
            throw new TypeError("Can not construct abstract class.");
        }
        if (this.getPrices === dataAbstract.prototype.getPrices) {
            throw new TypeError("Please implement abstract method getPrices.");
        }
    }
    getPrices() {
        throw new TypeError("Do not call abstract method getPrices from child.");
    }
}

module.exports = dataAbstract