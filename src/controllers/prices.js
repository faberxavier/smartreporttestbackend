const cache = require('./dataSource/pricesCache');
const openExchangeApi = require('./dataSource/openExchangeApi');
const { CACHE_EXPIRATION } = require('../config');
const { json } = require('express');

let lastCacheTimeStamp
let openApi
let pricesCache
class Prices {

    constructor() {
        openApi = new openExchangeApi()
        pricesCache = new cache().getInstance()
        lastCacheTimeStamp = pricesCache.getTime()
    }

    async getListPrices() {
        if (!this.validateCache()) {
            await this.updateCache()
        }
        return this.buildJsonReponse('USD', pricesCache.getPrices())
    }
    async getPricesByCache() {
        return this.buildJsonReponse('USD', pricesCache.getPrices())

    }
    async convert(value, fromCurrency, toCurrency) {
        if (!this.validateCache()) {
            await this.updateCache()
        }
        let prices = pricesCache.getPrices()
        let result = {
            rates: { [toCurrency]: this.calculateConversion(value, prices.rates[fromCurrency], prices.rates[toCurrency]) },
            timestramp: lastCacheTimeStamp
        }

        return this.buildJsonReponse(fromCurrency, result)
    }
    calculateConversion(value, priceFromCurrency, priceToCurrency) {
        return parseFloat(value / priceFromCurrency * priceToCurrency).toFixed(4)
    }
    validateCache() {
        return this.checkIfTimeIsNotOverLimit()
    }
    checkIfTimeIsNotOverLimit() {
        return (Date.now() - lastCacheTimeStamp) < CACHE_EXPIRATION;
    }
    buildJsonReponse(fromCurrency, prices) {
        let jsonReponse = { timestamp: lastCacheTimeStamp, rates: [] }
        for (const currency in prices.rates) {
            jsonReponse.rates.push({ "fromCurrency": fromCurrency, "toCurrency": currency, "price": prices.rates[currency] })
        }
        return jsonReponse
    }
    async updateCache() {
        let data = await openApi.getPrices()
        pricesCache.updateCache(data)
        lastCacheTimeStamp = pricesCache.getTime()
    }
}

module.exports = Prices