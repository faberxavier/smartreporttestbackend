const dataSource = require('./prices')
const erroMessage = "QUERY ERROR"
const listPrices = async (req, res) => {
    try {
        let data = new dataSource()
        data = await data.getListPrices()
        return res.status(200).send(data);
    } catch (error) {
        data = await data.getPricesByCache()
        return res.status(200).send(data);
    }
}

const convertCurrency = async (req, res) => {

    try {
        let result
        let data = new dataSource()
        if (verifyParam(req)) {
            result = await data.convert(parseFloat(req.query.value), req.query.fromCurrency, req.query.toCurrency)
            return res.status(200).send(result);
        }
        else
            return res.status(400).send(erroMessage);

    } catch (error) {
        console.log(error)
    }


}

verifyParam = (req) => {
    if (!req.query.value || !req.query.fromCurrency || !req.query.toCurrency) {
        return false
    }
    return true
}



module.exports = { listPrices, convertCurrency };

