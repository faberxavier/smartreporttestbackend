const assert = require('assert');
const Prices = require('../../controllers/prices');
const testData = require('../testData/testCache')
const cache = require('../../controllers/dataSource/pricesCache');
let pricesCache = new cache().getInstance()

describe('Test Prices Object ', () => {
    describe('Test time limit function', () => {
        it('should return false when cache timestamp is lower than now', () => {
            testData.timestamp = 10
            pricesCache.buildCacheInMemory(testData)
            let priceTest = new Prices()
            assert.equal(priceTest.checkIfTimeIsNotOverLimit(), false);
        });
        it('should return true when cache timestamp is higher than now', () => {
            testData.timestamp = Date.now() + 10000
            pricesCache.buildCacheInMemory(testData)
            let priceTest = new Prices()
            assert.equal(priceTest.checkIfTimeIsNotOverLimit(), true);
        });
        it('verify conversion between 2 currency get a amount value > 0', () => {
            let priceTest = new Prices()
            assert.equal(priceTest.calculateConversion(1, testData.rates['AED'], testData.rates['AFN']), 20.9640);
        });
        it('verify conversion between same currency', () => {
            let priceTest = new Prices()
            assert.equal(priceTest.calculateConversion(1, testData.rates['AED'], testData.rates['AED']), 1);
        });
        it('verify conversion between 2 currency get a amount value > 0 ', () => {
            let priceTest = new Prices()
            assert.equal(priceTest.calculateConversion(1, testData.rates['AOA'], testData.rates['ANG']), 0.0030);
        });
    })
});

