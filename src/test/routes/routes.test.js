var request = require('supertest');
describe('Server Routes Tests', function () {
    var server;
    beforeEach(() => {
        server = require('../../server');
    });
    afterEach(() => async (done) => {
        await server.close(done);
    });
    it('responds to /conversion', (done) => {
        request(server)
            .get('/')
            .expect(200, done);
    });
    it('404 everything else', (done) => {
        request(server)
            .get('/conversion')
            .expect(400, done);
    });
    it('All parameters [value, fromCurrency, toCurrency] server must respond', (done) => {
        request(server)
            .get('/conversion?value=100&fromCurrency=AMD&toCurrency=AED')
            .expect(200, done);
    });
    it('Miss parameter toCurrency server must respond error', (done) => {
        request(server)
            .get('/conversion?value=100&fromCurrency=AMD')
            .expect(400, done);
    });
    it('Miss parameter fromCurrency server must respond error', (done) => {
        request(server)
            .get('/conversion?value=100&toCurrency=AED')
            .expect(400, done);
    });
    it('Miss parameter value server must respond error', (done) => {
        request(server)
            .get('/conversion?fromCurrency=AMD&toCurrency=AED')
            .expect(400, done);
    });

});