require('dotenv-safe').config({
    allowEmptyValues: true,
    example: './src/config/env/.env.example',
    path: './src/config/env/.env'
});

module.exports = {
    PORT: process.env.PORT,
    API_ID: process.env.API_ID,
    CACHE_EXPIRATION: process.env.CACHE_EXPIRATION
};